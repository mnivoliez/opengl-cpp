#pragma once

#include <assimp/scene.h>
#include <string>
#include <vector>

#include "mesh.h"
#include "shader.h"


class Model {
public:
  /*  Functions   */
  Model(char *path) { loadModel(path); }
  void Draw(Shader shader);

private:
  /*  Model Data  */
  std::vector<Mesh> meshes;
  std::string directory;
  std::vector<Texture> textures_loaded;
  /*  Functions   */
  void loadModel(std::string path);
  void processNode(aiNode *node, const aiScene *scene);
  Mesh processMesh(aiMesh *mesh, const aiScene *scene);
  std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type,
                                            std::string typeName);
};
