#!/bin/bash
if [ ! -d "build" ]; then
  echo "Build directory does not exist."
  echo "Please, run Config.sh before running Build.sh."
  exit 1;
fi
if [ ! -d "build/src" ]; then
  echo "Build/src directory does not exist."
  echo "Please, run Build.sh before running RunApp.sh."
  exit 2;
fi
build/src/opg
