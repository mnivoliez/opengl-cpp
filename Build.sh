#!/bin/bash
if [ ! -d "build" ]; then
  echo "Build directory does not exist."
  echo "Please, run Config.sh before running Build.sh."
  exit 1;
fi
cmake --build build
exit 0
